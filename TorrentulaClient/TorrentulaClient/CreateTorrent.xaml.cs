﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TorrentulaClient
{
    public partial class CreateTorrent : Window
    {
        private Helper helper = new Helper();
        public CreateTorrent()
        {
            InitializeComponent();
        }

        /*
         * Function allows the user to choose a file to create a torrent for.
         */
        private void FilePathBrowse_Click(object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaOpenFileDialog dialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            dialog.Filter = "All files (*.*)|*.*";
            dialog.Multiselect = false;
            dialog.ShowDialog();

            FilePath.Text = dialog.FileName;
        }

        /*
         * Function allows the user to choose a folder to save the torrent file to.
         */
        private void SavePathBrowse_Click(object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaFolderBrowserDialog dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            dialog.Description = "Please select a folder to save the torrent file to.";
            dialog.UseDescriptionForTitle = true;
            dialog.ShowDialog();

            SavePath.Text = dialog.SelectedPath;
        }

        /*
         * Function sends the file and save path to the backend in order to create a
         * new torrent file, and closes the 'Create Torrent' window.
         */
        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            string request, response;

            if(FilePath.Text.Length > 0 && SavePath.Text.Length > 0)
            {
                // Set the message code to create torrent code.
                request = ((char)MessageCodes.CREATE_TORRENT_CODE).ToString().PadLeft(Constants.MESSAGE_CODE_LEN, '0');

                // Append the size of the data to the message.
                request += (FilePath.Text.Length + SavePath.Text.Length + 1).ToString().PadLeft(Constants.MESSAGE_SIZE_LEN, '0');

                // Append the request data to the message.
                request += FilePath.Text + ':' + SavePath.Text;

                // Send the message to the backend.
                helper.GetCommunicator().SendToBackend(request);

                // Get the response from the backend.
                response = helper.GetCommunicator().ReceiveFromBackend();

                // If the response is valid (message code is the expected one),
                // parse it and update the torrent files list.
                if (int.Parse(response.Substring(0, Constants.MESSAGE_CODE_LEN)) == (int)MessageCodes.OPEN_FILE_CODE)
                {
                    ((MainWindow)Application.Current.MainWindow).torrentFiles.Add(helper.FormatTorrentData(response));
                    ((MainWindow)Application.Current.MainWindow).TorrentFiles.Items.Refresh();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Unable to create torrent.");
                }
            }                        
        }

        /*
         * Function closes the 'Create Torrent' window.
         */
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
