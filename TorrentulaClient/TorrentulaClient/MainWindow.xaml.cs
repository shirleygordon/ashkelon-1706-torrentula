﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;

namespace TorrentulaClient
{
    public partial class MainWindow : Window
    {
        private Helper helper;
        public List<TorrentFile> torrentFiles = new List<TorrentFile>();
        private Thread t;

        public MainWindow()
        {
            Process backend = new Process();
            InitializeComponent();

            // Run the backend
            backend.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            backend.StartInfo.FileName = Constants.BACKEND_PATH;
            backend.Start();

            // Initialize helper object.
            helper = new Helper();

            torrentFiles.Add(new TorrentFile() { id = 1, name = "Hamilton", size = "5.3 GB", status = "Downloading", peers = 5 });
            torrentFiles.Add(new TorrentFile() { id = 2, name = "Love, Rosie", size = "2.5 GB", status = "Seeding", peers = 12 });
            torrentFiles.Add(new TorrentFile() { id = 3, name = "Extremely Wicked, Shockingly Evil and Vile", size = "4.2 GB", status = "Finished", peers = 0 });

            TorrentFiles.ItemsSource = torrentFiles;

            // Run thread to receive file status / peers updates.
            t = new Thread(new ThreadStart(GetTorrentUpdates));
            t.Start();
        }

        /*
         * Function listens for incoming updates from the backend,
         * and updates the torrent list accordingly. 
         */
        private void GetTorrentUpdates()
        {
            TorrentFile temp;
            UpdateMessage updateMessage;
            string message;
            int messageCode, index;

            while (true)
            {           
                // Receive the response from the server and parse it.
                message = helper.GetCommunicator().ReceiveFromBackend(true);

                if (message.Length > 0)
                {
                    messageCode = int.Parse(message.Substring(0, Constants.MESSAGE_CODE_LEN));

                    switch(messageCode)
                    {
                        case (int)MessageCodes.UPDATE_STATUS_CODE:
                            updateMessage = helper.FormatUpdateMessage(message);
                            index = torrentFiles.FindIndex(x => x.id == updateMessage.id);

                            if(index >= 0)
                            {
                                temp = torrentFiles[index];
                                temp.status = updateMessage.newValue;
                                torrentFiles[index] = temp;
                                TorrentFiles.Items.Refresh();
                            }
                            break;

                        case (int)MessageCodes.UPDATE_PEERS_CODE:
                            updateMessage = helper.FormatUpdateMessage(message);
                            index = torrentFiles.FindIndex(x => x.id == updateMessage.id);

                            if (index >= 0)
                            {
                                temp = torrentFiles[index];
                                temp.peers = int.Parse(updateMessage.newValue);
                                torrentFiles[index] = temp;
                                TorrentFiles.Items.Refresh();
                            }
                            break;
                    }                
                }

                Thread.Sleep(500);
            }
        }

        /*
         * Function lets the user select a torrent file, and sends the
         * file path to the backend to start downloading it.
         */
        private void MenuOpen_Click(object sender, RoutedEventArgs e)
        {
            string request, response;
            Ookii.Dialogs.Wpf.VistaOpenFileDialog dialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            dialog.Filter = "torrent files (*.torrent)|*.torrent";
            dialog.Multiselect = false;
            dialog.ShowDialog();

            // If a file was selected, send the file path to the backend.
            if(dialog.FileName != "" && dialog.FileName.EndsWith(".torrent"))
            {
                // Set the message code to open file code.
                request = ((char)MessageCodes.OPEN_FILE_CODE).ToString().PadLeft(Constants.MESSAGE_CODE_LEN, '0');

                // Append the size of the data to the message.
                request += dialog.FileName.Length.ToString().PadLeft(Constants.MESSAGE_SIZE_LEN, '0');

                // Append the request data to the message.
                request += dialog.FileName;

                // Send the message to the backend.
                helper.GetCommunicator().SendToBackend(request);

                // Get the response from the backend.
                response = helper.GetCommunicator().ReceiveFromBackend();

                // If the response is valid (message code is the expected one),
                // parse it and update the torrent files list.
                if(int.Parse(response.Substring(0, Constants.MESSAGE_CODE_LEN)) == (int)MessageCodes.OPEN_FILE_CODE)
                {
                    torrentFiles.Add(helper.FormatTorrentData(response));
                    TorrentFiles.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("Unable to open file:\n" + dialog.FileName);
                }
            }
        }

        /*
         * Function opens up a 'Create Torrent' popup window.
         */
        private void MenuCreateTorrent_Click(object sender, RoutedEventArgs e)
        {
            CreateTorrent popup = new CreateTorrent();
            popup.Show();
        }

        /*
         * Function sends an exit message to the backend and exits the program.
         */
        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            string request;

            // Set the message code to exit code.
            request = ((char)MessageCodes.EXIT_CODE).ToString().PadLeft(Constants.MESSAGE_CODE_LEN, '0');

            // Append the size of the data to the message.
            request += "0000";

            // Send the message to the backend.
            helper.GetCommunicator().SendToBackend(request);

            System.Environment.Exit(1);
        }
    }
}
