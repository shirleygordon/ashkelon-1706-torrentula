﻿namespace TorrentulaClient
{
    public static class Constants
    {
        public const int DATA_START_INDEX = 6;
        public const int MESSAGE_CODE_LEN = 2;
        public const int MESSAGE_SIZE_LEN = 4;
        public const string BACKEND_IP = "127.0.0.1";
        public const int BACKEND_PORT = 6666;
        public const int UPDATER_PORT = 7777;
        public const int BUFFER_SIZE = 1024;
        public const string BACKEND_PATH = "";
    }

    enum MessageCodes
    {
        OPEN_FILE_CODE,
        UPDATE_STATUS_CODE,
        UPDATE_PEERS_CODE,
        CREATE_TORRENT_CODE,
        EXIT_CODE
    }

    enum StatusCodes
    {
        FAILURE,
        SUCCESS
    }

    enum TorrentDataIndexes
    {
        ID,
        NAME,
        SIZE,
        STATUS,
        PEERS
    }
}