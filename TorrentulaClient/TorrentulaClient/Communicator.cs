﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Windows;

namespace TorrentulaClient
{
    public class Communicator
    {        
        private static TcpClient client;
        private static IPEndPoint backendEndPoint;
        private static NetworkStream clientStream;
        private static TcpClient updater;
        private static IPEndPoint updaterEndPoint;
        private static NetworkStream updaterStream;

        public Communicator()
        {
            try
            {
                client = new TcpClient();
                backendEndPoint = new IPEndPoint(IPAddress.Parse(Constants.BACKEND_IP), Constants.BACKEND_PORT);
                client.Connect(backendEndPoint);
                clientStream = client.GetStream();

                updater = new TcpClient();
                updaterEndPoint = new IPEndPoint(IPAddress.Parse(Constants.BACKEND_IP), Constants.UPDATER_PORT);
                updater.Connect(updaterEndPoint);
                updaterStream = updater.GetStream();
            }
            catch
            {
                MessageBox.Show("Unable to connect to backend.");
                System.Environment.Exit(1);
            }
        }

        /* Function sends the given string to the backend.
         * Input: data string.
         * Output: none.
         */
        public void SendToBackend(string data)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(data);

            try
            {
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch
            {
                MessageBox.Show("Unable to connect to backend.");
                System.Environment.Exit(1);
            }
        }

        /* Function reads data from the backend socket and returns it as string.
         * Input: none.
         * Output: data string.
         */
        public string ReceiveFromBackend(bool updater = false)
        {
            byte[] buffer = new byte[Constants.BUFFER_SIZE];

            try
            {
                if (!updater)
                {
                    int bytesRead = clientStream.Read(buffer, 0, Constants.BUFFER_SIZE);
                }
                else
                {
                    int bytesRead = updaterStream.Read(buffer, 0, Constants.BUFFER_SIZE);
                }
            }
            catch
            {
                MessageBox.Show("Unable to connect to backend.");
                System.Environment.Exit(1);
            }
            
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }
    }
}
