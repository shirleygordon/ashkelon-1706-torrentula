﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;

namespace TorrentulaClient
{
    public struct TorrentFile
    {
        public int id { get; set; }
        public string name { get; set; }
        public string size { get; set; }
        public string status { get; set; }
        public int peers { get; set; }
    }

    public struct UpdateMessage
    {
        public int id { get; set; }
        public string newValue { get; set; }
    }

    class Helper
    {
        static Communicator communicator = new Communicator();

        public Communicator GetCommunicator()
        {
            return communicator;
        }

        /*
         * Function turns a string separated by colons into a list.
         * Input: unformatted string.
         * Output: list containing the string's content.
         */
        public List<string> FormatList(string listStr)
        {
            // Remove the first 6 characters of the message (message code and size).
            listStr = listStr.Substring(Constants.DATA_START_INDEX);
            return listStr.Split(new string[] { ":" }, StringSplitOptions.None).ToList<string>();
        }

        /*
         * Function turns a string separated by colons into a torrent file struct.
         * Input: unformatted string.
         * Output: TorrentFile struct.
         */
        public TorrentFile FormatTorrentData(string torrentDataStr)
        {
            List<string> torrentDataList = FormatList(torrentDataStr);
            TorrentFile torrentData = new TorrentFile {  id = int.Parse(torrentDataList[(int)TorrentDataIndexes.ID]),
                                                name = torrentDataList[(int)TorrentDataIndexes.NAME],
                                                size = torrentDataList[(int)TorrentDataIndexes.SIZE],
                                                status = torrentDataList[(int)TorrentDataIndexes.STATUS],
                                                peers = int.Parse(torrentDataList[(int)TorrentDataIndexes.PEERS]) };
            
            return torrentData;
        }

        /*
         * Function turns a string separated by colons into an update message struct.
         * Input: unformatted string.
         * Output: UpdateMessage struct.
         */
        public UpdateMessage FormatUpdateMessage(string message)
        {
            List<string> updateMessageData = FormatList(message);
            UpdateMessage messageData = new UpdateMessage
            {
                id = int.Parse(updateMessageData[(int)TorrentDataIndexes.ID]),
                newValue = updateMessageData.Last()
            };

            return messageData;
        }
    }
}
