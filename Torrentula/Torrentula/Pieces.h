#pragma once
#include <vector>
#include <mutex>
#include "PieceBlockQueue.h"

class Pieces
{
private:
	std::vector<std::vector<bool>> _requested;
	std::vector<std::vector<bool>> _received;
	std::mutex _requestedMtx;
	std::mutex _receivedMtx;

public:
	Pieces(const Torrent& torrent);
	~Pieces() {};

	static std::vector<std::vector<bool>>& buildPiecesVector(const Torrent& torrent);
	void addRequested(const PieceBlock& pieceBlock);
	void addReceived(const PieceBlock& pieceBlock);
	bool isNeeded(const PieceBlock& pieceBlock);
	bool isDone();
	bool fullPieceReceived(const uint32_t& index);
};