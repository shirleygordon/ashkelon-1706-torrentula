#pragma once

#include <exception>
#include <string>

class ConnectionException : public std::exception
{
public:
	ConnectionException(const std::string& message) : _message(message) {}
	virtual ~ConnectionException() noexcept = default;
	virtual const char* what() const noexcept { return _message.c_str(); }

protected:
	std::string _message;
};