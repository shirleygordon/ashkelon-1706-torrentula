#include "DownloadManager.h"

/*
Function handles messages from a peer.
Input: peer socket.
Output: none.
*/
void DownloadManager::handlePeer(SOCKET socket)
{
    PieceBlockQueue piecesToRequest(m_torrent); // queue of pieces the peer has and have not yet been requested
    bool choked = true; // determines whether the connection is choked or not
    char* data;

    // Try to send and receive handshake message.
    try
    {
        Helper::sendData(socket, buildHandshake());
        data = Helper::extractHandshake(socket);
        handleMessage(data, socket, piecesToRequest, choked);
    }
    catch (std::exception& e) // If an error occurs, close the socket and return.
    {
        std::cerr << e.what() << std::endl;
        closesocket(socket);
        return;
    }

    // Start handling messages until the connection closes.
    while (true)
    {
        try
        {
            data = Helper::extractMessage(socket);

            // Handle the message.            
            try
            {
                handleMessage(data, socket, piecesToRequest, choked);
            }
            catch (std::exception& e) // If an error occurs, close the socket and return.
            {
                std::cerr << e.what() << std::endl;
                closesocket(socket);
                return;
            }            
        }
        catch (std::exception& e) // If an error occurs, close the socket and return.
        {
            std::cerr << e.what() << std::endl;
            closesocket(socket);
            return;
        }
    }
}

/*
Function checks for message type and handles it accordingly.
Input: message buffer, socket.
Output: none.
*/
void DownloadManager::handleMessage(char* message, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked)
{
    // If the peer sent a handshake message, respond with an interested message.
    if (isHandshake(message))
    {
        Helper::sendData(socket, buildIdMessage(INTERESTED));
    }
    else
    {
        Message response = parseMessage(message);

        switch (response.id)
        {
            case CHOKE:
                chokeHandler(socket, choked);
                break;

            case UNCHOKE:
                unchokeHandler(socket, piecesToRequest, choked);
                break;

            case HAVE:
                haveHandler(response.payload, socket, piecesToRequest);
                break;

            case BITFIELD:
                bitfieldHandler(response, socket, piecesToRequest);
                break;

            case PIECE:
                pieceHandler(response.payload, socket, piecesToRequest);
                break;

            default:
                break;
        }
    }
}

/*
Function checks if a message is a handshake message.
Input: message buffer.
Output: true if the message is a handshake message, false otherwise.
*/
bool DownloadManager::isHandshake(char* message) const
{
    return sizeof(message) == HANDSHAKE_LEN && std::string(&message[1], 19) == "BitTorrent protocol";
}

/*
Function handles a choke message.
Input: socket, choked.
Output: none.
*/
void DownloadManager::chokeHandler(SOCKET& socket, bool& choked) const
{
    choked = true;
}

/*
Function handles an unchoke message.
Input: socket, pieces to request, choked.
Output: none.
*/
void DownloadManager::unchokeHandler(SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked)
{
    choked = false;
    requestPiece(socket, piecesToRequest, choked);
}

/*
Function handles a have message.
Input: payload, socket, pieces to request, choked.
Output: none.
*/
void DownloadManager::haveHandler(std::map<std::string, char*> payload, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked)
{
    const uint32_t pieceIndex = Helper::bytesTo32bitInt(payload["index"]);
    const bool wasQueueEmpty = piecesToRequest.empty();

    piecesToRequest.enqueue(pieceIndex);

    // If this is the first piece in the queue, request it.
    // Otherwise, do nothing, as we want to wait for the piece
    // response to come back before requesting the next piece.
    if (wasQueueEmpty)
    {
        requestPiece(socket, piecesToRequest, choked);
    }
}

/*
Function handles a bitfield message.
Input: response, socket, pieces to request, choked.
Output: none.
*/
void DownloadManager::bitfieldHandler(Message& response, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked)
{
    const bool wasQueueEmpty = piecesToRequest.empty();
    char byte = 0;

    // Iterate over each bit in the payload and add pieces to the queue accordingly.
    // Each bit represents whether a piece at the bit's index is available.
    // 0 = not available. 1 = available.
    for (uint32_t i = 0; i < response.length - 1; i++)
    {
        byte = response.payload["bitfield"][i];

        for (uint32_t j = 0; j < 8; j++)
        {
            if (byte % 2)
            {
                piecesToRequest.enqueue(i * 8 + 7 - j);                
            }

            byte = (int)floor(byte / 2);
        }
    }

    // If the queue was previously empty, request a piece.
    // Otherwise, do nothing, as we want to wait for the piece
    // response to come back before requesting the next piece.
    if (wasQueueEmpty)
    {
        requestPiece(socket, piecesToRequest, choked);
    }
}

/*
Function handles a piece response and writes the received piece to file.
Input: response, socket, pieces to request, choked.
Output: none.
*/
void DownloadManager::pieceHandler(Message& response, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked)
{
    unsigned int offset = 0;
    PieceBlock pieceResp = { Helper::bytesTo32bitInt(response.payload["index"]),
                             Helper::bytesTo32bitInt(response.payload["begin"]),
                             Helper::bytesTo32bitInt(response.payload["length"]) };

    m_pieces.addReceived(pieceResp);

    // Calculate piece block offset and seek file
    offset = pieceResp.index * m_torrent.getPieceLength() + pieceResp.begin;
    
    m_fileMtx.lock();
    m_file.seekp(offset, std::ios::beg);

    // Write piece block to file
    m_file.write(response.payload["block"], sizeof(response.payload["block"]));
    m_fileMtx.unlock();

    // Check if the entire piece has been received.
    if (m_pieces.fullPieceReceived(pieceResp.index))
    {
        // If so, update the available pieces string in the database.
        m_db->updateAvailablePieces(pieceResp.index);
    }    

    if (m_pieces.isDone())
    {
        closesocket(socket);
    }
    else
    {
        requestPiece(socket, piecesToRequest, choked);
    }
}

/*
Function requests a piece from the peer.
Input: socket, queue of pieces to request, choked.
Output: none.
*/
void DownloadManager::requestPiece(SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked)
{
    PieceBlock pieceBlock = {0};
    bool requested = false;

    if (!choked)
    {
        // Pop pieces until a needed piece is found
        while (!piecesToRequest.empty() && !requested)
        {
            pieceBlock = piecesToRequest.deque();

            // Once a piece which is needed has been found, request the piece
            if (m_pieces.isNeeded(pieceBlock))
            {                
                Helper::sendData(socket, buildRequest(pieceBlock));
                m_pieces.addRequested(pieceBlock);
                requested = true;
            }
        }
    }
}

/*
Constructor for DownloadManager class.
Input: peers vector, reference to the torrent object, peer id, number of pieces.
Output: none.
*/
DownloadManager::DownloadManager(std::vector<Peer>& peers, Torrent& torrent, char* peerID, int numPieces) : m_peers(peers), m_pieces(numPieces)
{
    m_torrent = torrent;
    m_peerID = peerID;

    // Create output file if it doesn't already exist
    std::ofstream file(OUTPUT_FILES_PATH + torrent.getFileName(), std::ios::binary);
    file.close();
}

/*
Function creates a new thread for communicating with each peer,
and starts downloading the torrent from all the peers.
Input: none.
Output: none.
*/
void DownloadManager::startDownloading()
{
    int result;
    WSAInitializer wsaInit;
    
    // Open output file
    m_file.open(OUTPUT_FILES_PATH + m_torrent.getFileName(), std::ios::binary | std::ios::out | std::ios::in);

    // Iterate through all the peers
    for (Peer& peer : m_peers)
    {
        // Create a new socket for each peer
        SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        if (sock == INVALID_SOCKET)
            continue;

        // Initialize peer address struct
        struct sockaddr_in peerAddr = { 0 };
        peerAddr.sin_family = AF_INET;
        peerAddr.sin_addr.s_addr = inet_addr(peer.ip);
        peerAddr.sin_port = htons(peer.port);

        // Connect to peer
        result = connect(sock, (SOCKADDR*)&peerAddr, sizeof(peerAddr));

        if (result == SOCKET_ERROR)
        {      
            closesocket(sock);
            continue;
        }

        // Create a new thread to handle messages from each peer
        std::thread handler(&DownloadManager::handlePeer, this, sock);
        handler.detach();
    }
}

/*
Function creates a handshake message buffer.
Input: none.
Output: char array buffer containing the handshake message.
*/
char* DownloadManager::buildHandshake()
{
    char buffer[HANDSHAKE_LEN] = { 0 };

    Helper::insert8bit(19, buffer); // pstrlen
    memcpy(&buffer[1], "BitTorrent protocol", 19); // pstr
    Helper::insert32bit(0, &buffer[20]); // reserved
    Helper::insert32bit(0, &buffer[24]); // reserved
    memcpy(&buffer[28], m_torrent.getInfoHash(), INFO_HASH_LEN); // info hash
    memcpy(&buffer[48], m_peerID, PEER_ID_LEN); // peer id

    return buffer;
}

/*
Function creates a keep alive message buffer.
Input: none.
Output: char array buffer containing the keep alive message.
*/
char* DownloadManager::buildKeepAlive()
{
    char buffer[KEEP_ALIVE_LEN] = { 0 };
    return buffer;
}

/*
Function creates a choke/unchoke/interested/uninterested message buffer,
according to the message id.
Input: 8 bit message id.
Output: char array buffer containing the message.
*/
char* DownloadManager::buildIdMessage(uint8_t id)
{
    char buffer[ID_MESSAGE_LEN] = { 0 };

    Helper::insert32bit(1, buffer); // length
    Helper::insert8bit(id, &buffer[4]); // id

    return buffer;
}

/*
Function creates a have message buffer.
Input: 32 bit piece index.
Output: char array buffer containing the have message.
*/
char* DownloadManager::buildHave(uint32_t pieceIndex)
{
    char buffer[HAVE_LEN] = { 0 };

    Helper::insert32bit(5, buffer); // length
    Helper::insert8bit(HAVE, &buffer[4]); // id
    Helper::insert32bit(pieceIndex, &buffer[5]); // piece index

    return buffer;
}

/*
Function creates a bitfield message buffer.
Input: bitfield char array.
Output: char array buffer containing the bitfield message.
*/
char* DownloadManager::buildBitfield(char* bitfield)
{
    std::vector<char> buffer;
    char temp[BITFIELD_LEN] = { 0 };
    unsigned int i = 0;

    // Set the length and id properties and push them into the vector
    Helper::insert32bit(sizeof(bitfield) + 1, temp); // length
    Helper::insert8bit(BITFIELD, &temp[4]); // id
    for (i = 0; i < BITFIELD_LEN; i++)
    {
        buffer.push_back(temp[i]);
    }

    // Insert the bitfield array into the vector
    for (i = 0; i < sizeof(bitfield); i++)
    {
        buffer.push_back(bitfield[i]);
    }

    return buffer.data();
}

/*
Function creates a request message buffer.
Input: piece block.
Output: char array buffer containing the request message.
*/
char* DownloadManager::buildRequest(const PieceBlock& pieceBlock)
{
    char buffer[REQUEST_LEN] = { 0 };

    Helper::insert32bit(13, buffer); // message length
    Helper::insert8bit(REQUEST, &buffer[4]); // id
    Helper::insert32bit(pieceBlock.index, &buffer[5]); // piece index
    Helper::insert32bit(pieceBlock.begin, &buffer[9]); // begin
    Helper::insert32bit(pieceBlock.length, &buffer[13]); // length

    return buffer;
}

/*
Function creates a piece message buffer.
Input: block char array, 32 bit index and begin.
Output: char array buffer containing the piece message.
*/
char* DownloadManager::buildPiece(char* block, uint32_t index, uint32_t begin)
{
    std::vector<char> buffer;
    char temp[PIECE_LEN] = { 0 };
    unsigned int i = 0;

    // Set the length and id properties and push them into the vector
    Helper::insert32bit(sizeof(block) + 9, temp); // length
    Helper::insert8bit(PIECE, &temp[4]); // id
    Helper::insert32bit(index, &temp[5]); // piece index
    Helper::insert32bit(begin, &temp[9]); // begin
    for (i = 0; i < PIECE_LEN; i++)
    {
        buffer.push_back(temp[i]);
    }

    // Insert the piece block into the vector
    for (i = 0; i < sizeof(block); i++)
    {
        buffer.push_back(block[i]);
    }

    return buffer.data();
}

/*
Function creates a cancel message buffer.
Input: piece block.
Output: char array buffer containing the cancel message.
*/
char* DownloadManager::buildCancel(const PieceBlock& pieceBlock)
{
    char buffer[CANCEL_LEN] = { 0 };

    Helper::insert32bit(13, buffer); // message length
    Helper::insert8bit(CANCEL, &buffer[4]); // id
    Helper::insert32bit(pieceBlock.index, &buffer[5]); // piece index
    Helper::insert32bit(pieceBlock.begin, &buffer[9]); // begin
    Helper::insert32bit(pieceBlock.length, &buffer[13]); // length

    return buffer;
}

/*
Function creates a port message buffer.
Input: 16 bit port number.
Output: char array buffer containing the port message.
*/
char* DownloadManager::buildPort(uint16_t port)
{
    char buffer[PORT_LEN] = { 0 };

    Helper::insert32bit(3, buffer); // message length
    Helper::insert8bit(PORT, &buffer[4]); // id
    Helper::insert16bit(port, &buffer[5]); // port number

    return buffer;
}

/*
Function creates a Message struct from a message buffer.
Input: char buffer.
Output: Message struct containing the parsed message.
*/
Message DownloadManager::parseMessage(char* buffer)
{
    Message message;

    // Extract the message length and id.
    message.length = Helper::bytesTo32bitInt(buffer);
    message.id = sizeof(buffer) > KEEP_ALIVE_LEN ? (uint8_t)buffer[4] : NULL;

    // If the message is one of the messages which have a payload, extract the payload.
    if (message.id == REQUEST || message.id == PIECE || message.id == CANCEL)
    {
        strncat(message.payload["index"], &buffer[5], 4);
        strncat(message.payload["begin"], &buffer[9], 4);
        message.payload[message.id == PIECE ? "block" : "length"] = &buffer[13];
    }
    else if (message.id == HAVE || message.id == BITFIELD)
    {
        message.payload[message.id == HAVE ? "index" : "bitfield"] = &buffer[5];
    }

    return message;
}