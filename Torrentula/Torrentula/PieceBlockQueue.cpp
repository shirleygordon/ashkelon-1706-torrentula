#include "PieceBlockQueue.h"

/*
Function adds a new piece to the queue.
Input: piece index.
Output: none.
*/
void PieceBlockQueue::enqueue(const uint32_t& pieceIndex)
{
	const unsigned int numBlocks = _torrent.blocksPerPiece(pieceIndex);

	for (unsigned int i = 0; i < numBlocks; i++)
	{
		const PieceBlock pieceBlock = { pieceIndex, i * BLOCK_LEN, _torrent.blockLen(pieceIndex, i) };
		_queue.push(pieceBlock);
	}
}

/*
Function removes and returns the first piece in the queue.
Input: none.
Output: the first piece in the queue.
*/
PieceBlock& PieceBlockQueue::deque()
{
	PieceBlock& pieceBlock = _queue.front();
	_queue.pop();
	return pieceBlock;
}

/*
Function returns the first piece in the queue.
Input: none.
Output: the first piece in the queue.
*/
PieceBlock PieceBlockQueue::peek() const
{
	return _queue.front();
}

/*
Function checks whether the queue is empty.
Input: none.
Output: true if the queue is empty, false otherwise.
*/
bool PieceBlockQueue::empty() const
{
	return _queue.size() == 0;
}
