#include "Helper.h"

/*
Function extracts a message from the socket according to bytesNum.
Input: socket, bytesNum (number of bytes to extract).
Output: char array containing the message.
*/
char* Helper::getPartFromSocket(SOCKET sc, uint32_t bytesNum)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];

	int res = 0;

	res = recv(sc, data, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

/*
Function inserts an 8 bit integer into a string buffer.
Input: 8 bit integer, reference to string buffer.
Output: none.
*/
void Helper::insert8bit(const uint8_t val, char* buffer)
{
	buffer[0] = char((val & 0xFF) >> 0);
}

/*
Function inserts a 16 bit integer into a string buffer.
Input: 16 bit integer, reference to string buffer.
Output: none.
*/
void Helper::insert16bit(const uint16_t val, char* buffer)
{
	buffer[0] = char((val & 0xFF00) >> 8);
	buffer[1] = char((val & 0x00FF) >> 0);
}

/*
Function inserts a 32 bit integer into a string buffer.
Input: 32 bit integer, reference to string buffer.
Output: none.
*/
void Helper::insert32bit(const uint32_t val, char* buffer)
{
	buffer[0] = char((val & 0xFF000000) >> 24);
	buffer[1] = char((val & 0x00FF0000) >> 16);
	buffer[2] = char((val & 0x0000FF00) >> 8);
	buffer[3] = char((val & 0x000000FF) >> 0);
}

/*
Function inserts a 64 bit integer into a string buffer.
Input: 64 bit integer, reference to string buffer.
Output: none.
*/
void Helper::insert64bit(const uint64_t val, char* buffer)
{
	buffer[0] = char((val & 0xFF00000000000000) >> 56);
	buffer[1] = char((val & 0x00FF000000000000) >> 48);
	buffer[2] = char((val & 0x0000FF0000000000) >> 40);
	buffer[3] = char((val & 0x000000FF00000000) >> 32);
	buffer[4] = char((val & 0x00000000FF000000) >> 24);
	buffer[5] = char((val & 0x0000000000FF0000) >> 16);
	buffer[6] = char((val & 0x000000000000FF00) >> 8);
	buffer[7] = char((val & 0x00000000000000FF) >> 0);
}

/*
Function converts a char array to a 32 bit integer.
Input: char buffer.
Output: 32 bit integer.
*/
uint32_t Helper::bytesTo32bitInt(char* buffer)
{
	uint32_t num = (uint32_t)buffer[0] << 24 |
		(uint32_t)buffer[1] << 16 |
		(uint32_t)buffer[2] << 8 |
		(uint32_t)buffer[3];
	return num;
}

/*
Function extracts a handshake message from the socket.
Input: socket.
Output: handshake message buffer.
*/
char* Helper::extractHandshake(SOCKET socket)
{
	char* data = getPartFromSocket(socket, HANDSHAKE_MSG_LEN);
	return data;
}

/*
Function extracts a message from the socket.
Input: socket.
Output: one whole message buffer.
*/
char* Helper::extractMessage(SOCKET socket)
{
	// Extract the length of the message (first 32 bits)
	 uint32_t length = get32bitIntFromSocket(socket);

	// Based on the length, extract the rest of the message
	char* data = getPartFromSocket(socket, length);

	return data;
}

/*
Function sends a message to a peer.
Input: socket, char message buffer.
Output: none.
*/
void Helper::sendData(SOCKET sc, char* message)
{
	if (send(sc, message, sizeof(message), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to peer.");
	}
}