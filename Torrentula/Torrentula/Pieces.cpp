#include "Pieces.h"

/*
Constructor for Pieces class.
Input: torrent object.
Output: none.
*/
Pieces::Pieces(const Torrent& torrent)
{
	_requested = buildPiecesVector(torrent);
	_received = buildPiecesVector(torrent);
}

/*
Function builds a 2d vector of bool to hold piece blocks.
Input: none.
Output: 2d vector of bool.
*/
std::vector<std::vector<bool>>& Pieces::buildPiecesVector(const Torrent& torrent)
{
	std::vector<std::vector<bool>> vec;
	const unsigned int numPieces = torrent.getNumPieces();

	// For each piece index, insert a vector whose size is the number of blocks per that piece.
	for (unsigned int i = 0; i < numPieces; i++)
	{
		vec.push_back(std::vector<bool>(torrent.blocksPerPiece(i), false));
	}

	return vec;
}

/*
Function adds a piece to the requested pieces vector.
Input: piece index.
Output: none.
*/
void Pieces::addRequested(const PieceBlock& pieceBlock)
{
	const int blockIndex = pieceBlock.begin / BLOCK_LEN;
	std::lock_guard<std::mutex> lock(_requestedMtx);
	_requested[pieceBlock.index][blockIndex] = true;
}

/*
Function adds a piece to the received pieces vector.
Input: piece index.
Output: none.
*/
void Pieces::addReceived(const PieceBlock& pieceBlock)
{
	const int blockIndex = pieceBlock.begin / BLOCK_LEN;
	std::lock_guard<std::mutex> lock(_receivedMtx);
	_received[pieceBlock.index][blockIndex] = true;
}

/*
Function checks if a piece is needed.
Input: piece index.
Output: true if the piece is needed, false otherwise.
*/
bool Pieces::isNeeded(const PieceBlock& pieceBlock)
{
	const int blockIndex = pieceBlock.begin / BLOCK_LEN;
	std::vector<std::vector<bool>>::const_iterator i;
	bool allRequested = true;
	std::lock_guard<std::mutex> lockReq(_requestedMtx);

	// If all the pieces have been requested
	for (i = _requested.begin(); i != _requested.end() && allRequested; i++)
	{
		if (std::find(i->begin(), i->end(), false) != i->end())
		{
			allRequested = false;			
		}
	}

	if (allRequested)
	{
		// Copy the received vector into the requested vector in order to
		// rerequest pieces which haven't been received.
		std::lock_guard<std::mutex> lockRec(_receivedMtx);
		_requested = _received;
	}

	return !_requested[pieceBlock.index][blockIndex];
}

/*
Function checks if all the pieces have been received.
Input: none.
Output: true if all the pieces have been received, false otherwise.
*/
bool Pieces::isDone()
{
	std::vector<std::vector<bool>>::const_iterator i;
	bool allReceived = true;
	std::lock_guard<std::mutex> lock(_receivedMtx);

	for (i = _received.begin(); i != _received.end() && allReceived; i++)
	{
		if (std::find(_received.begin(), _received.end(), false) != _received.end())
		{
			allReceived = false;
		}
	}
	return allReceived;
}

/*
Function checks whether all blocks of a piece have been received.
Input: piece index.
Output: true if full piece has been received, false otherwise.
*/
bool Pieces::fullPieceReceived(const uint32_t& index)
{
	bool isFullPiece = true;

	for (unsigned int i = 0; i < _received[index].size() && isFullPiece; i++)
	{
		if (!_received[index][i])
		{
			isFullPiece = false;
		}
	}

	return false;
}
