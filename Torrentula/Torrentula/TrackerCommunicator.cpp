#include "TrackerCommunicator.h"

/*
Function sends a connect request to a tracker,
receives a connect response and extracts the connection ID.
Input: reference to udp socket, reference to tracker endpoint.
Output: connection ID.
*/
uint32_t& TrackerCommunicator::connect(SOCKET& socket, const sockaddr_in& trackerEndpoint) const
{
    ConnectRequest request = { 0x041727101980, rand() };
    char requestBuffer[CONNECT_SIZE] = { 0 };
    char responseBuffer[CONNECT_RESPONSE_BUFFER_SIZE] = {0};
    int endpointLen = sizeof(trackerEndpoint), socketTimeout = 0;
    uint32_t action = 0, transactionID = 0, connectionID = 0;


    // Create connect request buffer
    insert64bit(request.connectionID, &requestBuffer[0]);
    insert32bit(CONNECT_ACTION_CODE /*action=connect*/, &requestBuffer[8]);
    insert32bit(request.transactionID, &requestBuffer[12]);

    // Send connect request and receive response buffer.
    memcpy(responseBuffer, sendAndReceive(socket, trackerEndpoint, requestBuffer), CONNECT_RESPONSE_BUFFER_SIZE);
    
    // Validate connect response
    if(strlen((char*)responseBuffer) >= 16)
    {
        // Extract the different parts of the response
        bitsToInt(action, responseBuffer);
        bitsToInt(transactionID, responseBuffer + 4);
        bitsToInt(connectionID, responseBuffer + 8);

        if (action != CONNECT_ACTION_CODE || request.transactionID != transactionID)
        {
            throw ConnectionException("Error connecting to tracker.");
        }
    }
    else
    {
        throw ConnectionException("Error connecting to tracker.");
    }

    return connectionID;
}

/*
Function sends an announce request to a tracker,
receives an announce response and extracts the peers.
Input: reference to udp socket, reference to tracker endpoint, reference to connection id.
Output: none.
*/
void TrackerCommunicator::announce(SOCKET& socket, const sockaddr_in& trackerEndpoint, const uint32_t& connectionID)
{
    char requestBuffer[ANNOUNCE_SIZE];
    char responseBuffer[ANNOUNCE_RESPONSE_BUFFER_SIZE] = { 0 };
    uint32_t action = 0, transactionID = rand(), serverTransactionID = 0;

    // Create announce request buffer
    insert64bit(connectionID, &requestBuffer[0]);
    insert32bit(ANNOUNCE_ACTION_CODE /*action=announce*/, &requestBuffer[8]);
    insert32bit(transactionID, &requestBuffer[12]);
    memcpy(&requestBuffer[16], _infoHash, INFO_HASH_LEN);
    memcpy(&requestBuffer[36], _peerID, PEER_ID_LEN);
    insert64bit(0 /*downloaded=0*/, &requestBuffer[56]);
    insert64bit(_left, &requestBuffer[64]);
    insert64bit(0 /*uploaded=0*/, &requestBuffer[72]);
    insert32bit(0 /*event=none*/, &requestBuffer[80]);
    insert32bit(0 /*ip_address=default*/, &requestBuffer[84]);
    insert32bit(rand() /*key=random*/, &requestBuffer[88]);
    insert32bit(-1 /*num_want=default*/, &requestBuffer[92]);
    insert16bit(6881, &requestBuffer[96]);

    // Send announce request and receive response buffer.
    memcpy(responseBuffer, sendAndReceive(socket, trackerEndpoint, requestBuffer), ANNOUNCE_RESPONSE_BUFFER_SIZE);

    // Validate announce response
    if (strlen((char*)responseBuffer) >= 20)
    {
        // Extract the different parts of the response
        bitsToInt(action, responseBuffer);
        bitsToInt(serverTransactionID, responseBuffer + 4);

        if (action != ANNOUNCE_ACTION_CODE || serverTransactionID != transactionID)
        {
            throw ConnectionException("Error connecting to tracker.");
        }
        else
        {
            extractPeers(responseBuffer);
        }
    }
    else
    {
        throw ConnectionException("Error connecting to tracker.");
    }
}

/*
Function extracts the peers from the announce response and adds them
to the peers vector of the TrackerCommunicator class.
Input: announce response string.
Output: none.
*/
void TrackerCommunicator::extractPeers(char* peersList)
{
    Peer newPeer = {"", 0};
    unsigned int i = 20, j = 0;
    uint32_t seeders = bitsToInt(seeders, &peersList[16]);

    if (seeders > 0)
    {
        while (i + 5 < strlen(peersList))
        {
            newPeer.ip = "";

            // Extract the ip address and put it into a string
            for (j = i; j < i + 4; j++)
            {
                newPeer.ip += (int)peersList[j];

                if (j != i + 3)
                {
                    newPeer.ip += '.';
                }
            }

            // Extract the port number
            newPeer.port = bitsToInt(newPeer.port, &peersList[i + 4]);

            // Add the new peer to the peers vector
            _peers.push_back(newPeer);

            i += 6;
        }
    }
}

/*
Function generates a peer id for the client.
Input: none.
Output: 20 byte char array with the generated peer id.
*/
char* TrackerCommunicator::generatePeerID()
{
    char peerID[PEER_ID_LEN] = {0};

    // Peer ID is a 20 byte string which contains a fixed part and a random part.
    // In our case, the fixed part is "-TT0001-" (8 bytes), and the next 12 bytes are random.
    memcpy(peerID, "-TT0001-", 8);
    insert64bit(rand(), &peerID[8]);
    insert32bit(rand(), &peerID[16]);

    return peerID;
}

/*
Function inserts a 64 bit integer into a buffer.
Input: 64 bit integer, buffer pointer.
Output: none.
*/
void TrackerCommunicator::insert64bit(const uint64_t val, char* buffer)
{
    buffer[0] = char((val & 0xFF00000000000000) >> 56);
    buffer[1] = char((val & 0x00FF000000000000) >> 48);
    buffer[2] = char((val & 0x0000FF0000000000) >> 40);
    buffer[3] = char((val & 0x000000FF00000000) >> 32);
    buffer[4] = char((val & 0x00000000FF000000) >> 24);
    buffer[5] = char((val & 0x0000000000FF0000) >> 16);
    buffer[6] = char((val & 0x000000000000FF00) >> 8);
    buffer[7] = char((val & 0x00000000000000FF) >> 0);
}

/*
Function inserts a 32 bit integer into a buffer.
Input: 32 bit integer, buffer pointer.
Output: none.
*/
void TrackerCommunicator::insert32bit(const uint32_t val, char* buffer)
{
    buffer[0] = char((val & 0xFF000000) >> 24);
    buffer[1] = char((val & 0x00FF0000) >> 16);
    buffer[2] = char((val & 0x0000FF00) >> 8);
    buffer[3] = char((val & 0x000000FF) >> 0);
}

/*
Function inserts a 16 bit integer into a buffer.
Input: 16 bit integer, buffer pointer.
Output: none.
*/
void TrackerCommunicator::insert16bit(const uint16_t val, char* buffer)
{
    buffer[0] = char((val & 0xFF00) >> 8);
    buffer[1] = char((val & 0x00FF) >> 0);
}

/*
Function sets a timeout for the recvfrom function of a UDP socket.
Input: reference to socket, sec, usec.
Output: -1 if error occured, 0 if the request timed out, > 0 if data is ready to be read.
*/
int TrackerCommunicator::recvfromTimeOutUDP(SOCKET& socket, long sec, long usec)
{
    // Setup timeval variable
    timeval timeout;
    timeout.tv_sec = sec;
    timeout.tv_usec = usec;

    // Setup fd_set structure
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(socket, &fds);

    return select(0, &fds, 0, 0, &timeout);
}

/*
Function sends a request and receives a response from the tracker.
Input: socket, tracker endpoint and request buffer.
Output: response buffer.
*/
char* TrackerCommunicator::sendAndReceive(SOCKET& socket, const sockaddr_in& trackerEndpoint, char* requestBuffer)
{
    char responseBuffer[ANNOUNCE_RESPONSE_BUFFER_SIZE] = { 0 };    
    int endpointLen = sizeof(trackerEndpoint), socketTimeout = 0;

    // Send request to tracker
    if (sendto(socket, requestBuffer, strlen(requestBuffer), 0, (struct sockaddr*)&trackerEndpoint, endpointLen) == SOCKET_ERROR)
    {
        throw SocketException("sendto() failed with error code: " + WSAGetLastError());
    }

    // Set socket timeout and try to receive response
    socketTimeout = recvfromTimeOutUDP(socket, 10, 0);

    switch (socketTimeout)
    {
    case 0:
        throw SocketException("Socket timed out.");
        break;

    case -1:
        throw SocketException("Socket error occured: " + WSAGetLastError());
        break;

    default:
        // Receive response
        if (recvfrom(socket, responseBuffer, ANNOUNCE_RESPONSE_BUFFER_SIZE, 0, (struct sockaddr*)&trackerEndpoint, &endpointLen) == SOCKET_ERROR)
        {
            throw SocketException("recvfrom() failed with error code: " + WSAGetLastError());
        }
    }

    return responseBuffer;
}

/*
Function converts an array of chars to an integer.
Input: reference to an integer variable, array of chars.
Output: integer.
*/
template <typename IntegerType>
IntegerType TrackerCommunicator::bitsToInt(IntegerType& result, const char* bits)
{
    result = 0;

    for (unsigned n = 0; n < sizeof(result); n++)
    {
        result = (result << 8) + bits[n];
    }        

    return result;
}

/*
Constructor for TrackerCommunicator object.
Input: map of trackers, info hash and left.
Output: none.
*/
TrackerCommunicator::TrackerCommunicator(std::map<std::string, unsigned int>& trackers, const uint8_t* infoHash, const uint64_t left) : _trackers(trackers)
{
    memcpy(_infoHash, infoHash, INFO_HASH_LEN);
    memcpy(_peerID, generatePeerID(), PEER_ID_LEN);
    _left = left;
}

/*
Function gets peers for a torrent from all the available trackers.
Input: none.
Output: none.
*/
void TrackerCommunicator::getPeers()
{
    std::map<std::string, unsigned int>::iterator it;
    struct sockaddr_in trackerEndpoint;
    SOCKET sock;
    uint32_t connectionID = 0;
    WSAInitializer wsaInit;
    HOSTENT* ip;

    // Create socket
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
    {
        printf("socket() failed with error code : %d", WSAGetLastError());
        exit(EXIT_FAILURE);
    }

    // Setup address structure
    memset((char*)&trackerEndpoint, 0, sizeof(trackerEndpoint));
    trackerEndpoint.sin_family = AF_INET;

    // Get peers from each tracker
    for (it = _trackers.begin(); it != _trackers.end(); it++)
    {
        // Get the tracker's IP address
        ip = gethostbyname(it->first.c_str());

        // If IP address was found
        if (ip != nullptr)
        {
            // Setup the port and IP address
            trackerEndpoint.sin_port = htons(it->second);
            trackerEndpoint.sin_addr.S_un.S_addr = inet_addr(ip->h_addr_list[0]);

            try
            {
                // Send connect request and receive connection ID
                connectionID = connect(sock, trackerEndpoint);

                // Send announce request and receive peers
                announce(sock, trackerEndpoint, connectionID);
            }
            catch (std::exception& e) // Socket timed out
            {
                std::cerr << e.what() << std::endl;
            }
        }
    }
    
    closesocket(sock);
}
