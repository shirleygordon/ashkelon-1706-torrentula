#include "DatabaseWrapper.h"

/*
Constructor for DatabaseWrapper class.
Input: none.
Output: none.
*/
DatabaseWrapper::DatabaseWrapper()
{
	_database = new SqliteDatabase();
	_database->open();
}

/*
Destructor for DatabaseWrapper class.
Input: none.
Output: none.
*/
DatabaseWrapper::~DatabaseWrapper()
{
	_database->close();
	delete _database;
	_database = nullptr;
}
