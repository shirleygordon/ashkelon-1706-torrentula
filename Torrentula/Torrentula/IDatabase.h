#pragma once
#include <string>
#include <map>
#include <vector>
#include <list>

typedef struct {
	std::string path;
	size_t size;
} file_t;

typedef struct TorrentFile
{
	std::list<std::string> m_announcers;
	std::list<file_t> m_files;

	size_t m_piece_size;
	std::list<std::string> m_pieces;
};

class IDatabase
{
private:
public:
	virtual ~IDatabase() = default;

	virtual bool open() = 0;
	virtual void close() = 0;

	virtual void addNewFile(const TorrentFile& file) const = 0;
};
