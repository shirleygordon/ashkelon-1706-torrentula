#pragma once
#include <list>
#include <vector>
#include <string>
#include <fstream>
#include <numeric>
#include <WinSock2.h>
#include "IDatabase.h"
#include "bencode.hpp"
#include "TrackerCommunicator.h"
#include "DownloadManager.h"
#include "UploadManager.h"

using namespace std;

#define MAIN_PORT 6666
#define UPDATE_PORT 7777

static const std::string UDP_URI = "udp://";
static const size_t HASH_LEN = 20;

class ClientManager {
private:
	std::vector<TorrentFile> m_torrentFiles;
	SOCKET m_mainSocket;
	SOCKET m_updateSocket;
	TrackerCommunicator m_trackerCommunicator;
	DownloadManager m_downloadManager;
	UploadManager m_uploadManager;

	void addTorrent(const std::string& path);
	void initializeSocket(SOCKET& backendSocket);
	void bindAndListen(SOCKET& backendSocket, unsigned int port);
	SOCKET accept(SOCKET& backendSocket);

public:
	ClientManager();
	~ClientManager();
	void start();
};
