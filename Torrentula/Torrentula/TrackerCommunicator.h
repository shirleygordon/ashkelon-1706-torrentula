#pragma once
#include <string>
#include <map>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <WinSock2.h>

#include "WSAInitializer.h"
#include "SocketException.h"
#include "ConnectionException.h"

#define CONNECT_ACTION_CODE 0
#define ANNOUNCE_ACTION_CODE 1
#define LISTENING_PORT 6881
#define CONNECT_SIZE 16
#define ANNOUNCE_SIZE 98
#define CONNECT_RESPONSE_BUFFER_SIZE 128
#define ANNOUNCE_RESPONSE_BUFFER_SIZE 2048
#define INFO_HASH_LEN 20
#define PEER_ID_LEN 20

typedef struct ConnectRequest
{
	uint64_t connectionID;
	uint32_t transactionID;
} ConnectRequest;

typedef struct Peer
{
	std::string ip;
	uint16_t port;
} Peer;

class TrackerCommunicator
{
private:
	std::map<std::string, unsigned int>& _trackers;
	std::vector<Peer> _peers;
	char _infoHash[INFO_HASH_LEN];
	char _peerID[PEER_ID_LEN];
	uint64_t _left;
	
	uint32_t& connect(SOCKET& socket, const sockaddr_in& trackerEndpoint) const;
	void announce(SOCKET& socket, const sockaddr_in& trackerEndpoint, const uint32_t& connectionID);
	void extractPeers(char* peersList);

	// Helper functions
	static char* generatePeerID();
	static void insert64bit(const uint64_t val, char* buffer);
	static void insert32bit(const uint32_t val, char* buffer);
	static void insert16bit(const uint16_t val, char* buffer);
	static int recvfromTimeOutUDP(SOCKET& socket, long sec, long usec);
	static char* sendAndReceive(SOCKET& socket, const sockaddr_in& trackerEndpoint, char* requestBuffer);
	template <typename IntegerType>	static IntegerType bitsToInt(IntegerType& result, const char* bits);

public:
	TrackerCommunicator(std::map<std::string, unsigned int>& trackers, const uint8_t* infoHash, const uint64_t left);
	~TrackerCommunicator() {};
	
	void getPeers();
};