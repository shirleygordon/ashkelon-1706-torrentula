#pragma once

#include <map>
#include <WinSock2.h>
#include <fstream>
#include <iostream>
#include <thread>
#include <sstream>
#include "DownloadManager.h"
#include "DatabaseWrapper.h"
#include "Helper.h"

#define HANDSHAKE_START_LEN 28

typedef struct File
{
	char infoHash[INFO_HASH_LEN];
	std::ifstream& file;
	size_t pieceLength;
} File;

class UploadManager
{
private:
	SOCKET m_serverSocket;
	DatabaseWrapper m_db;
	//std::map<char[], std::ifstream> m_files;
	std::vector<File> m_files;
	std::mutex m_fileMtx;

	void accept();
	void handleClient(SOCKET socket, File& file);
	void handleMessage(SOCKET& socket, char* message, File& file);
	void sendHaveMessages(SOCKET& socket, File& file);

	void interestedHandler(SOCKET& socket);
	void uninterestedHandler(SOCKET& socket);
	void requestHandler(Message request, SOCKET& socket, File& file);

public:
	UploadManager();
	~UploadManager();

	void serve();
};