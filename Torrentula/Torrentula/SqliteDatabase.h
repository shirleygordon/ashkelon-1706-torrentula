#pragma once
#include <io.h>
#include <iostream>
#include "IDatabase.h"
#include "sqlite3.h"

class SqliteDatabase : public IDatabase
{
private:
	sqlite3* _db;
	std::string _dbFileName;

	void initDatabase() const;

public:
	SqliteDatabase();
	~SqliteDatabase();

	bool open();
	void close();

	void addNewFile(const TorrentFile& file) const;
};