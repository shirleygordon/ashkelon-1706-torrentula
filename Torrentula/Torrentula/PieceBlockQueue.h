#pragma once
#include <queue>
#include "Torrent.h"

typedef struct PieceBlock {
	uint32_t index;
	uint32_t begin;
	uint32_t length;
} PieceBlock;

class PieceBlockQueue
{
private:
	Torrent& _torrent;
	std::queue<PieceBlock> _queue;

public:
	PieceBlockQueue(Torrent& torrent) : _torrent(torrent) {};
	~PieceBlockQueue() = default;

	void enqueue(const uint32_t& pieceIndex);
	PieceBlock& deque();
	PieceBlock peek() const;
	bool empty() const;
};