#pragma once
#include <string>
#include <vector>
#include <WinSock2.h>

#define HANDSHAKE_MSG_LEN 68

class Helper
{
private:
	static char* getPartFromSocket(SOCKET sc, uint32_t bytesNum);

public:
	static void insert8bit(const uint8_t val, char* buffer);
	static void insert16bit(const uint16_t val, char* buffer);
	static void insert32bit(const uint32_t val, char* buffer);
	static void insert64bit(const uint64_t val, char* buffer);

	static uint32_t bytesTo32bitInt(char* buffer);

	static char* extractHandshake(SOCKET socket);
	static char* extractMessage(SOCKET socket);
	static void sendData(SOCKET sc, char* message);
};