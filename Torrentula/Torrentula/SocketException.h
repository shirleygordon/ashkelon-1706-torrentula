#pragma once

#include <exception>
#include <string>

class SocketException : public std::exception
{
public:
	SocketException(const std::string& message) : _message(message) {}
	virtual ~SocketException() noexcept = default;
	virtual const char* what() const noexcept { return _message.c_str(); }

protected:
	std::string _message;
};