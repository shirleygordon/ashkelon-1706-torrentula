#include "ClientManager.h"

/*
Function adds a new torrent file.
Input: file path.
Output: none.
*/
void ClientManager::addTorrent(const std::string& path)
{
	ifstream torrent_file(path, std::ios::binary | std::ios::ate);

	std::list<std::string> m_announcers;
	std::list<file_t> m_files;

	size_t m_piece_size;
	std::list<std::string> m_pieces;

	vector<char> buffer(torrent_file.tellg());
	torrent_file.seekg(0, ios::beg);

	if (!torrent_file.read(buffer.data(), buffer.size()))
	{
		throw;
	}

	auto root = get<bencode::dict>(bencode::decode(string(buffer.begin(), buffer.end())));
	auto info = get<bencode::dict>(root["info"]);

	// init announcers
	for (auto&& announcer : get<bencode::list>(root["announce-list"]))
	{
		for (auto&& url : get<bencode::list>(announcer))
		{
			auto s = get<bencode::string>(url);
			for_each(s.begin(),
				s.end(),
				[](auto&& ch)
				{
					ch = static_cast<char>(tolower(ch));
				});

			// only add udp announcers
			const auto url_start = s.find_first_not_of(UDP_URI);
			if (url_start == UDP_URI.size())
			{
				m_announcers.emplace_back(s.substr(url_start));
			}
		}
	}

	for (auto&& file_obj : get<bencode::list>(info["files"]))
	{
		auto&& file_dict = get<bencode::dict>(file_obj);
		auto&& path_list = get<bencode::list>(file_dict["path"]);
		file_t file = {};

		file.size = get<bencode::integer>(file_dict["length"]);
		file.path = accumulate(++path_list.begin(),
			path_list.end(),
			get<bencode::string>(path_list[0]),
			[](auto&& left, auto&& right) {
				return left + "/" + get<bencode::string>(right);
			});

		m_files.emplace_back(file);
	}

	auto pieces = get<bencode::string>(info["pieces"]);
	while (!pieces.empty())
	{
		m_pieces.push_back(pieces.substr(0, HASH_LEN));
		pieces.erase(0, HASH_LEN);
	}

	m_piece_size = get<bencode::integer>(info["piece length"]);

	m_torrentFiles.push_back({ m_announcers, m_files, m_piece_size,	m_pieces });
}

/*
Function initializes a TCP socket.
Input: reference to socket.
Output: none.
*/
void ClientManager::initializeSocket(SOCKET& backendSocket)
{
	backendSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (backendSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*
Function binds a socket and starts listening on it.
Input: reference to backend socket, port number to bind the socket to.
Output: none.
*/
void ClientManager::bindAndListen(SOCKET& backendSocket, unsigned int port)
{
	struct sockaddr_in sa = { 0 };

	// Initialize socket address
	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	// Bind - connect between the socket and the configuration (port and ip)
	if (bind(backendSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " binded");

	// Start listening for incoming requests
	if (listen(backendSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " listening...");

	std::cout << "Listening on port " << port << "..." << std::endl;
}

/*
Function accepts a new client connection.
Input: reference to backend socket.
Output: frontend socket.
*/
SOCKET ClientManager::accept(SOCKET& backendSocket)
{
	// Accept the client and create a new socket for this client.
	SOCKET frontendSocket = ::accept(m_mainSocket, NULL, NULL);

	if (frontendSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	return frontendSocket;
}

/*
Constructor for ClientManager class.
Input: none.
Output: none.
*/
ClientManager::ClientManager() : m_trackerCommunicator(), m_downloadManager(), m_uploadManager()
{
	// Initialize both sockets
	initializeSocket(m_mainSocket);
	initializeSocket(m_updateSocket);
}

/*
Destructor for ClientManager class.
Input: none.
Output: none.
*/
ClientManager::~ClientManager()
{
	try
	{
		closesocket(m_mainSocket);
		closesocket(m_updateSocket);
	}
	catch (...) {}
}

/*
Function listens for incoming requests from the GUI.
Input: none.
Output: none.
*/
void ClientManager::start()
{
	WSAInitializer wsaInit;
	SOCKET frontendMainSocket;
	SOCKET frontendUpdateSocket;

	// Bind the sockets and start listening for frontend connection
	bindAndListen(m_mainSocket, MAIN_PORT);
	bindAndListen(m_updateSocket, UPDATE_PORT);

	// Accept frontend connection
	frontendMainSocket = accept(m_mainSocket);
 	frontendUpdateSocket = accept(m_updateSocket);
}
