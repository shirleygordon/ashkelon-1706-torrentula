#include "SqliteDatabase.h"

using namespace std;

/*
Function initializes a new database for the program.
Input: none.
Output: none.
*/
void SqliteDatabase::initDatabase() const
{
	
}


/*
Constructor for SqliteDatabase class.
Input: none.
Output: none.
*/
SqliteDatabase::SqliteDatabase()
{
	_db = nullptr;
	_dbFileName = "TorrentulaDB.sqlite";
}

/*
Destructor for SqliteDatabase class.
Input: none.
Output: none.
*/
SqliteDatabase::~SqliteDatabase()
{
	this->close();
	delete _db;
	_db = nullptr;
}

/*
Function opens the database file.
Input: none.
Output: true if the database was opened successfully, false otherwise.
*/
bool SqliteDatabase::open()
{
	int doesFileExist = _access(_dbFileName.c_str(), 0);
	int res = sqlite3_open(_dbFileName.c_str(), &(this->_db));

	if (res != SQLITE_OK) //If an error occurred on opening
	{
		this->_db = nullptr;
		cout << "Failed to open DB" << endl;
		return false;
	}

	if (doesFileExist == 0)//Create all tables if the database did not exist
	{

		const char* sqlStatement_files = "CREATE TABLE FILES (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, PATH TEXT NOT NULL, SIZE INTEGER NOT NULL);";
		const char* sqlStatement_torrent_files = "CREATE TABLE TORRENT_FILES (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ANNOUNCE TEXT NOT NULL, LENGTH INTEGER NOT NULL,NAME TEXT NOT NULL, PIECE_LENGTH INTEGER NOT NULL,FOREIGN KEY (ID) REFERENCES FILES(ID));";
		char** errMessage = nullptr;

		res = sqlite3_exec(_db, sqlStatement_files, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;

		res = sqlite3_exec(_db, sqlStatement_torrent_files, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
	}

	return true;
}

/*
Function closes the database file.
Input: none.
Output: none.
*/
void SqliteDatabase::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}


void SqliteDatabase::addNewFile(const TorrentFile& file) const
{
	int res = 0;
	//string add_picture = "INSERT INTO FILES (PATH, SIZE) SELECT '" + file.files.first + "'," + to_string(file.files.second) + ";";//("INSERT INTO PICTURES (ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID) SELECT %d,'%s','%s','%s',ID FROM ALBUMS.NAME == '%s';",picture.getId(),picture.getName() ,picture.getPath(), picture.getCreationDate(), albumName);
	//string add_picture = "INSERT INTO TORRENT_FILES (ANNOUNCE, LENGTH, PIECE_LENGTH) SELECT '" + file.announce + "'," + to_string(file.length) + "," + to_string(file.pieceLength) + ";";//("INSERT INTO PICTURES (ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID) SELECT %d,'%s','%s','%s',ID FROM ALBUMS.NAME == '%s';",picture.getId(),picture.getName() ,picture.getPath(), picture.getCreationDate(), albumName);
	char** errMessage = nullptr;

	//res = sqlite3_exec(_db, add_picture.c_str(), nullptr, nullptr, errMessage);//Query run
	cout << errMessage << std::endl;
}

