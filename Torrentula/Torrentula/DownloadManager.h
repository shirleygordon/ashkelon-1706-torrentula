#pragma once
#include <vector>
#include <string>
#include <thread>
#include <iostream>
#include <map>
#include <queue>
#include <fstream>
#include "TrackerCommunicator.h"
#include "Helper.h"
#include "Pieces.h"
#include "PieceBlockQueue.h"

#define HANDSHAKE_LEN 68
#define KEEP_ALIVE_LEN 4
#define ID_MESSAGE_LEN 5
#define HAVE_LEN 9
#define BITFIELD_LEN 5
#define REQUEST_LEN 17
#define PIECE_LEN 13
#define CANCEL_LEN 17
#define PORT_LEN 7
#define OUTPUT_FILES_PATH "../Output Files/"

enum messageID : uint8_t { CHOKE, UNCHOKE, INTERESTED, UNINTERESTED, HAVE, BITFIELD, REQUEST, PIECE, CANCEL, PORT };

typedef struct Message {
	uint32_t length;
	uint8_t id;
	std::map<std::string, char*> payload;
} Message;

class DownloadManager
{
private:
	std::vector<Peer> m_peers;
	Torrent& m_torrent;
	char m_peerID[PEER_ID_LEN];
	Pieces m_pieces;
	std::fstream m_file;
	std::mutex m_fileMtx;
	DatabaseWrapper m_db;

	// Handlers
	void handlePeer(SOCKET socket);
	void handleMessage(char* message, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked);
	bool isHandshake(char* message) const;

	// Specific message handlers
	void chokeHandler(SOCKET& socket, bool& choked) const;
	void unchokeHandler(SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked);
	void haveHandler(std::map<std::string, char*> payload, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked);
	void bitfieldHandler(Message& response, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked);
	void pieceHandler(Message& response, SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked);
	void requestPiece(SOCKET& socket, PieceBlockQueue& piecesToRequest, bool& choked);

public:
	DownloadManager(std::vector<Peer>& peers, Torrent& torrent, char* peerID, int numPieces);
	~DownloadManager() {};

	void startDownloading();

	// Message builder functions
	static char* buildHandshake();
	static char* buildKeepAlive();
	static char* buildIdMessage(uint8_t id);
	static char* buildHave(uint32_t pieceIndex);
	static char* buildBitfield(char* bitfield);
	static char* buildRequest(const PieceBlock& pieceBlock);
	static char* buildPiece(char* block, uint32_t index, uint32_t begin);
	static char* buildCancel(const PieceBlock& pieceBlock);
	static char* buildPort(uint16_t port);

	static Message parseMessage(char* buffer);
};