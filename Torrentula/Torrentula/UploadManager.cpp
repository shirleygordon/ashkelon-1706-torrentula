#include "UploadManager.h"

/*
Constructor for UploadManager class.
Input: none.
Output: none.
*/
UploadManager::UploadManager()
{
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*
Destructor for UploadManager class.
Input: none.
Output: none.
*/
UploadManager::~UploadManager()
{
	try
	{
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

/*
Function listens for incoming client connections.
Input: none.
Output: none.
*/
void UploadManager::serve()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(6881); // Port that the server will listen for.
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr("127.0.0.1"); // Server ip.

	// Stepping out to the global namespace.
	// Connects between the socket and the configuration (port and etc..).
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients.
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening..." << std::endl;

	while (true)
	{
		// The main thread accepts new peer connections.
		std::cout << "Waiting for peer connection request..." << std::endl;

		// Accept connection with new peer and create new peer thread.
		accept();
	}
}

/*
Function accepts new client connections and creates a thread for them.
Input: none.
Output: none.
*/
void UploadManager::accept()
{
	char* data;
	char infoHash[INFO_HASH_LEN] = { 0 };
	std::string filePath = "";
	std::ifstream file;
	bool match = true;	

	// Accept the client and create a specific socket from server to this client.
	SOCKET clientSocket = ::accept(m_serverSocket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	// Get the handshake message from the client socket.
	data = Helper::extractHandshake(clientSocket);

	// Extract info hash and peer id from message.
	memcpy(infoHash, &data[HANDSHAKE_START_LEN], INFO_HASH_LEN);

	// Check if info hash matches any of the open files.
	for (auto& fileElement : m_files) {
		for (unsigned int i = 0; i < INFO_HASH_LEN && match; i++)
		{
			if (infoHash[i] != fileElement.infoHash[i])
			{
				match = false;
			}
		}
		// If yes, create a new thread for the peer with a reference to the file,
		// and the peer's id.
		if (match)
		{
			std::thread clientThread(&UploadManager::handleClient, this, clientSocket, fileElement);
			clientThread.detach();
			break;
		}

		match = true;
	}

	// If not, check if info hash exists in the database.
	filePath = m_db->_database->getFilePath(infoHash);

	// If yes, open the file and add it to the map, and create a new thread for the peer
	// with a reference to the file and the peer's id.
	if (filePath != "")
	{
		file.open(filePath, std::ios::in || std::ios::binary);

		if (file)
		{
			size_t pieceLen = m_db->_database->getPieceLen(filePath);
			File newFile = { infoHash, file, pieceLen };
			m_files.push_back(newFile);
			std::thread clientThread(&UploadManager::handleClient, this, clientSocket, newFile);
			clientThread.detach();
		}
		else
		{
			closesocket(clientSocket); // Close the client's socket.
		}
	}
	else // If not, close the connection.
	{
		closesocket(clientSocket); // Close the client's socket.
	}
}

/*
Function handles a conversation with a client.
Input: client socket, reference to file.
Output: none.
*/
void UploadManager::handleClient(SOCKET socket, File& file)
{
	char* data;

	try
	{
		// Send handshake message to client.
		Helper::sendData(socket, DownloadManager::buildHandshake());

		// Send have messages to client.
		sendHaveMessages(socket, file);

		while (socket != INVALID_SOCKET)
		{
			// Get message from client.
			data = Helper::extractMessage(socket);

			// Handle the message.            
			try
			{
				handleMessage(socket, data);
			}
			catch (std::exception& e) // If an error occurs, close the socket and return.
			{
				std::cerr << e.what() << std::endl;
				closesocket(socket);
				return;
			}
		}

		closesocket(socket);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		closesocket(socket);
	}
}

/*
Function checks for message type and handles it accordingly.
Input: socket, message buffer.
Output: none.
*/
void UploadManager::handleMessage(SOCKET& socket, char* message, File& file)
{
	Message request = DownloadManager::parseMessage(message);

	switch (request.id)
	{
	case INTERESTED:
		interestedHandler(socket);
		break;

	case UNINTERESTED:
		uninterestedHandler(socket);
		break;

	case REQUEST:
		requestHandler(request, socket, file);
		break;

	default:
		break;
	}
}

/*
Function sends have messages to client to inform them of the available pieces.
Input: socket, file struct reference.
Output: none.
*/
void UploadManager::sendHaveMessages(SOCKET& socket, File& file)
{
	std::string pieces = m_db->_database->getAvailablePieces(file.infoHash);

	for (uint32_t i = 0; i < pieces.size(); i++)
	{
		if (pieces[i] == '1')
		{
			Helper::sendData(socket, DownloadManager::buildHave(i));
		}
	}
}

/*
Function handles an interested message.
Input: socket.
Output: none.
*/
void UploadManager::interestedHandler(SOCKET& socket)
{
	// Send unchoke message to client.
	Helper::sendData(socket, DownloadManager::buildIdMessage(UNCHOKE));
}

/*
Function handles an uninterested message.
Input: socket.
Output: none.
*/
void UploadManager::uninterestedHandler(SOCKET& socket)
{
	// If the peer is uninterested, send a choke message and close the connection.
	Helper::sendData(socket, DownloadManager::buildIdMessage(CHOKE));
	throw std::exception("Peer is not interested.");
}

/*
Function handles an request message.
Input: request, socket, file struct reference.
Output: none.
*/
void UploadManager::requestHandler(Message request, SOCKET& socket, File& file)
{
	PieceBlock pieceReq = { Helper::bytesTo32bitInt(request.payload["index"]),
							 Helper::bytesTo32bitInt(request.payload["begin"]),
							 Helper::bytesTo32bitInt(request.payload["length"]) };
	char* block = new char[pieceReq.length];

	// Calculate piece block offset and seek file
	unsigned int offset = pieceReq.index * file.pieceLength + pieceReq.begin;

	m_fileMtx.lock();
	file.file.seekg(offset, std::ios::beg);

	// Read piece block from file
	file.file.read(block, pieceReq.length);
	m_fileMtx.unlock();

	// Send piece response to client
	Helper::sendData(socket, DownloadManager::buildPiece(block, pieceReq.index, pieceReq.begin));

	delete[] block;
}